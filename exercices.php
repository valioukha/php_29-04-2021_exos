<?php
//------------------------------------------------------------------------------
// Script pour exécuter des exercices en PHP via le terminal (php exercices.php)
// Créé par Valioukha
// Pour la formation DWWB 2021
// Changelog :
// v1.0 :
// - Ecriture du script complet avec quelques optimisations déjà effectuées
//------------------------------------------------------------------------------

// Intro
intro:
echo "\n\033[35mBienvenue sur le choix des exercices !\033[0m\n\n";

// Question sur le choix des exercices
echo "\033[34mTout d'abords, quel exercice souhaitez-vous faire ?\033[0m\n\n";

echo "Exercice \033[32m1.a\033[0m (en toute lettres), version longue. \033[34mTapez :\033[0m\033[32m 1.a \033[0m\n";
echo "Exercice \033[32m1.b\033[0m (en toute lettres), version optimisée. \033[34mTapez :\033[0m\033[32m 1.b \033[0m\n";
echo "Exercice \033[32m2.1\033[0m (range 1). \033[34mTapez :\033[0m\033[32m 2.1 \033[0m\n";
echo "Exercice \033[32m2.2\033[0m (range 2). \033[34mTapez :\033[0m\033[32m 2.2 \033[0m\n";
echo "Exercice \033[32m2.3\033[0m (range 3). \033[34mTapez :\033[0m\033[32m 2.3 \033[0m\n";
echo "Exercice \033[32m2.4\033[0m (range 4). \033[34mTapez :\033[0m\033[32m 2.4 \033[0m\n";
echo "Exercice \033[32m3\033[0m (divisible). \033[34mTapez :\033[0m\033[32m 3 \033[0m\n";
echo "Exercice \033[32m4\033[0m (nombre premier). \033[34mTapez :\033[0m\033[32m 4 \033[0m\n";
echo "Exercice \033[32m5.1\033[0m (table de multiplications 1). \033[34mTapez :\033[0m\033[32m 5.1 \033[0m\n";
echo "Exercice \033[32m5.2\033[0m (table de multiplications 2). \033[34mTapez :\033[0m\033[32m 5.2 \033[0m\n";
echo "Exercice \033[32m5.3\033[0m (table de multiplications 3). \033[34mTapez :\033[0m\033[32m 5.3 \033[0m\n";
echo "Exercice \033[32m6\033[0m (#). \033[34mTapez :\033[0m\033[32m 6 \033[0m\n";
echo "Exercice \033[32m7\033[0m (suite de Fibonacci). \033[34mTapez :\033[0m\033[32m 7 \033[0m\n";
echo "Exercice \033[32m8\033[0m (20 premiers nombre premiers). \033[34mTapez :\033[0m\033[32m 8 \033[0m\n\n";

echo "\033[32m";

$userdata = readline("Choix : ");

echo "\033[0m";

if ($userdata == "1.a") {
    $exercice = "1.a";
} elseif ($userdata == "1.b") {
    $exercice = "1.b";
} elseif ($userdata == "2.1") {
    $exercice = "2.1";
} elseif ($userdata == "2.2") {
    $exercice = "2.2";
} elseif ($userdata == "2.3") {
    $exercice = "2.3";
} elseif ($userdata == "2.4") {
    $exercice = "2.4";
} elseif ($userdata == 3) {
    $exercice = 3;
} elseif ($userdata == 4) {
    $exercice = 4;
} elseif ($userdata == "5.1") {
    $exercice = "5.1";
} elseif ($userdata == "5.2") {
    $exercice = "5.2";
} elseif ($userdata == "5.3") {
    $exercice = "5.3";
} elseif ($userdata == 6) {
    $exercice = 6;
} elseif ($userdata == 7) {
    $exercice = 7;
} elseif ($userdata == 8) {
    $exercice = 8;
} else {
    $exercice = "error";
}

// Redirection vers le choix des exercices
if ($exercice == "1.a") {
    goto exercice1_a;
} elseif ($exercice == "1.b") {
    goto exercice1_b;
} elseif ($exercice == "2.1") {
    goto exercice2_1;
} elseif ($exercice == "2.2") {
    goto exercice2_2;
} elseif ($exercice == "2.3") {
    goto exercice2_3;
} elseif ($exercice == "2.4") {
    goto exercice2_4;
} elseif ($exercice == 3) {
    goto exercice3;
} elseif ($exercice == 4) {
    goto exercice4;
} elseif ($exercice == "5.1") {
    goto exercice5_1;
} elseif ($exercice == "5.2") {
    goto exercice5_2;
} elseif ($exercice == "5.3") {
    goto exercice5_3;
} elseif ($exercice == 6) {
    goto exercice6;
} elseif ($exercice == 7) {
    goto exercice7;
} elseif ($exercice == 8) {
    goto exercice8;
} else {
    echo "\033[31mMalheureusement vous n'avez pas choisi d'exercice !\033[0m\n";
    goto choix;
}

// Redémarrage ou non du choix
choix:
echo "\n\033[34mSouhaitez-vous quitter le choix des exercices ?\033[0m (\033[32moui\033[0m / \033[32mnon\033[0m)\n\n";

echo "\033[32m";

$exitchoice = readline("\nRéponse : ");

echo "\033[0m";

if ($exitchoice == "oui" || $exitchoice == "o" || $exitchoice == "yes" || $exitchoice == "y") {
    echo "\n\033[34mAU REVOIR\033[0m\n\n";
    exit;
} elseif ($exitchoice == "non" || $exitchoice == "n" || $exitchoice == "no") {
    goto intro;
} else {
    echo "\033[31mErreur.\033[0m\n";
    goto choix;
}
//

/* Début du code de l'exercice 1.a (en toute lettres) version longue */
exercice1_a:

echo "\n\033[34mVeuillez choisir un nombre compris entre 1 et 20.\033[0m\n\n";

echo "\033[32m";

$nombre_utilisateur = intval(readline("Nombre : "));

echo "\033[0m";

echo "\nVotre nombre s'écrit : "; //à déplacer pour optimiser

switch ($nombre_utilisateur){
    case 0:
        echo "\033[32mzéro\033[0m\n";
        break;
    case 1:
        echo "\033[32mun\033[0m\n";
        break;
    case 2:
        echo "\033[32mdeux\033[0m\n";
        break;
    case 3:
        echo "\033[32mtrois\033[0m\n";
        break;
    case 4:
        echo "\033[32mquatre\033[0m\n";
        break;
    case 5:
        echo "\033[32mcinq\033[0m\n";
        break;
    case 6:
        echo "\033[32msix\033[0m\n";
        break;
    case 7:
        echo "\033[32msept\033[0m\n";
        break;
    case 8:
        echo "\033[32mhuit\033[0m\n";
        break;
    case 9:
        echo "\033[32mneuf\033[0m\n";
        break;
    case 10:
        echo "\033[32mdix\033[0m\n";
        break;
    case 11:
        echo "\033[32monze\033[0m\n";
        break;
    case 12:
        echo "\033[32mdouze\033[0m\n";
        break;
    case 13:
        echo "\033[32mtreize\033[0m\n";
        break;
    case 14:
        echo "\033[32mquatorze\033[0m\n";
        break;
    case 15:
        echo "\033[32mquinze\033[0m\n";
        break;
    case 16:
        echo "\033[32mseize\033[0m\n";
        break;
    case 17:
        echo "\033[32mdix-sept\033[0m\n";
        break;
    case 18:
        echo "\033[32mdix-huit\033[0m\n";
        break;
    case 19:
        echo "\033[32mdix-neuf\033[0m\n";
        break;
    case 20:
        echo "\033[32mvingt\033[0m\n";
        break;
    default:
        echo "\n\033[31mNous n'avons pas compris votre nombre !\033[0m\n";
}
/* Fin du code de l'exercice 1.a (en toute lettres) version longue */

reboot1_a:
// Redémarrage ou non de l'exercice 1.a (en toute lettres) version longue
echo "\n\033[34mSouhaitez-vous quitter l'exercice 1.a (en toute lettres) version longue ?\033[0m (\033[32moui\033[0m / \033[32mnon\033[0m)\n\n";
echo "\033[32m";
$exit = readline("\nRéponse : ");
echo "\033[0m";
if ($exit == "oui" || $exit == "o" || $exit == "yes" || $exit == "y") {
    goto choix;
} elseif ($exit == "non" || $exit == "n" || $exit == "no") {
    goto exercice1_a;
} else {
    echo "\033[31mErreur.\033[0m\n";
    goto reboot1_a;
}
//

/* Début du code de l'exercice 1.b (en toute lettres) version optimisée */
exercice1_b:

echo "\n\033[34mVeuillez choisir un nombre compris entre 1 et 20.\033[0m\n\n";

echo "\033[32m";

$nombre_utilisateur = intval(readline("Nombre : "));

echo "\033[0m";

echo "\nVotre nombre s'écrit : "; //à déplacer pour optimiser

switch ($nombre_utilisateur){
    case 0:
        echo "\033[32mzéro\033[0m\n";
        break;
    case 1:
        echo "\033[32mun\033[0m\n";
        break;
    case 2:
        echo "\033[32mdeux\033[0m\n";
        break;
    case 3:
        echo "\033[32mtrois\033[0m\n";
        break;
    case 4:
        echo "\033[32mquatre\033[0m\n";
        break;
    case 5:
        echo "\033[32mcinq\033[0m\n";
        break;
    case 6:
        echo "\033[32msix\033[0m\n";
        break;
    case 17:
        echo "\033[32mdix-\033[0m";
    case 7:
        echo "\033[32msept\033[0m\n";
        break;
    case 18:
        echo "\033[32mdix-\033[0m";
    case 8:
        echo "\033[32mhuit\n";
        break;
    case 19:
        echo "\033[32mdix-\033[0m";
    case 9:
        echo "\033[32mneuf\033[0m\n";
        break;
    case 10:
        echo "\033[32mdix\033[0m\n";
        break;
    case 11:
        echo "\033[32monze\033[0m\n";
        break;
    case 12:
        echo "\033[32mdouze\033[0m\n";
        break;
    case 13:
        echo "\033[32mtreize\033[0m\n";
        break;
    case 14:
        echo "\033[32mquatorze\033[0m\n";
        break;
    case 15:
        echo "\033[32mquinze\033[0m\n";
        break;
    case 16:
        echo "\033[32mseize\033[0m\n";
        break;
    case 20:
        echo "\033[32mvingt\033[0m\n";
        break;
    default:
        echo "\n\033[31mNous n'avons pas compris votre nombre !\033[0m\n";
}
/* Fin du code de l'exercice 1.b (en toute lettres) version optimisée */
// Le code peut sûrement être encore mieux optimisé.

reboot1_b:
// Redémarrage ou non de l'exercice 1.b (en toute lettres) version optimisée
echo "\n\033[34mSouhaitez-vous quitter l'exercice 1.b (en toute lettres) version optimisée ?\033[0m (\033[32moui\033[0m / \033[32mnon\033[0m)\n\n";
echo "\033[32m";
$exit = readline("\nRéponse : ");
echo "\033[0m";
if ($exit == "oui" || $exit == "o" || $exit == "yes" || $exit == "y") {
    goto choix;
} elseif ($exit == "non" || $exit == "n" || $exit == "no") {
    goto exercice1_b;
} else {
    echo "\033[31mErreur.\033[0m\n";
    goto reboot1_b;
}
//

/* Début du code de l'exercice 2.1 (range 1) */
exercice2_1:

$bonjour = "Bonjour";

echo "\n";

for ($i = 1; $i<=100; $i++) {
    echo $bonjour . "\n";
}
/* Fin du code de l'exercice 2.1 (range 1) */

reboot2_1:
// Redémarrage ou non de l'exercice 2.1 (range 1)
echo "\n\033[34mSouhaitez-vous quitter l'exercice 2.1 (range 1) ?\033[0m (\033[32moui\033[0m / \033[32mnon\033[0m)\n\n";
echo "\033[32m";
$exit = readline("\nRéponse : ");
echo "\033[0m";
if ($exit == "oui" || $exit == "o" || $exit == "yes" || $exit == "y") {
    goto choix;
} elseif ($exit == "non" || $exit == "n" || $exit == "no") {
    goto exercice2_1;
} else {
    echo "\033[31mErreur.\033[0m\n";
    goto reboot2_1;
}
//

/* Début du code de l'exercice 2.2 (range 2) */
exercice2_2:

echo "\n";

for ($i = 1; $i<=100; $i++) {
    echo $i . "\n";
}
/* Fin du code de l'exercice 2.2 (range 2) */

reboot2_2:
// Redémarrage ou non de l'exercice 2.2 (range 2)
echo "\n\033[34mSouhaitez-vous quitter l'exercice 2.2 (range 2) ?\033[0m (\033[32moui\033[0m / \033[32mnon\033[0m)\n\n";
echo "\033[32m";
$exit = readline("\nRéponse : ");
echo "\033[0m";
if ($exit == "oui" || $exit == "o" || $exit == "yes" || $exit == "y") {
    goto choix;
} elseif ($exit == "non" || $exit == "n" || $exit == "no") {
    goto exercice2_2;
} else {
    echo "\033[31mErreur.\033[0m\n";
    goto reboot2_2;
}
//

/* Début du code de l'exercice 2.3 (range 3) */
exercice2_3:

echo "\n\033[34mVeuillez choisir UN nombre.\033[0m\n\n";

echo "\033[32m";

$nombre_initial = 0;
$nombre_utilisateur = intval(readline("Nombre : "));
echo "\n";

echo "\033[0m";

if ($nombre_initial == $nombre_utilisateur) {
    echo "\033[31mVous n'avez pas choisi de nombre différent de 0 !\033[0m\n";
    goto reboot2_3;
} elseif (($nombre_initial - $nombre_utilisateur) == 1) {
    echo "\033[34mVous n'avez pas de nombre entre votre nombre choisi " . $nombre_utilisateur . " et 0 !\033[0m\n";
    goto reboot2_3;
} else {
    echo "\nVoici la liste des nombres entre votre nombre choisi " . $nombre_utilisateur . " et 0 :\n";

    $i = 1;
    while (($nombre_initial + $i) < $nombre_utilisateur) {
        echo "\n\033[32m" . ($nombre_initial + $i) . "\033[0m\n";
        $i++;
    }
}
/* Fin du code de l'exercice 2.3 (range 3) */

reboot2_3:
// Redémarrage ou non de l'exercice 2.3 (range 3)
echo "\n\033[34mSouhaitez-vous quitter l'exercice 2.3 (range 3) ?\033[0m (\033[32moui\033[0m / \033[32mnon\033[0m)\n\n";
echo "\033[32m";
$exit = readline("\nRéponse : ");
echo "\033[0m";
if ($exit == "oui" || $exit == "o" || $exit == "yes" || $exit == "y") {
    goto choix;
} elseif ($exit == "non" || $exit == "n" || $exit == "no") {
    goto exercice2_3;
} else {
    echo "\033[31mErreur.\033[0m\n";
    goto reboot2_3;
}
//

/* Début du code de l'exercice 2.4 (range 4) */
exercice2_4:

echo "\n\033[34mVeuillez choisir DEUX nombres (le premier devra être plus petit que le deuxième).\033[0m\n\n";

echo "\033[32m";

$nombre_utilisateur1 = intval(readline("Nombre 1 : "));
echo "\n";
$nombre_utilisateur2 = intval(readline("Nombre 2 : "));

echo "\033[0m";

if ($nombre_utilisateur1 == $nombre_utilisateur2) {
    echo "\033[31mVous n'avez pas choisi DEUX nombres différents !\033[0m\n";
    goto reboot2_4;
} elseif (($nombre_utilisateur2 - $nombre_utilisateur1) == 1) {
    echo "\033[34mVous n'avez pas de nombre entre vos deux nombres choisis !\033[0m\n";
    goto reboot2_4;
} else {
    echo "\nVoici la liste des nombres entre vos deux nombres choisis :\n";

    $i = 1;
    while (($nombre_utilisateur1 + $i) < $nombre_utilisateur2) {
        echo "\n\033[32m" . ($nombre_utilisateur1 + $i) . "\033[0m\n";
        $i++;
    }
}
/* Fin du code de l'exercice 2.4 (range 4) */

reboot2_4:
// Redémarrage ou non de l'exercice 2.4 (range 4)
echo "\n\033[34mSouhaitez-vous quitter l'exercice 2.4 (range 4) ?\033[0m (\033[32moui\033[0m / \033[32mnon\033[0m)\n\n";
echo "\033[32m";
$exit = readline("\nRéponse : ");
echo "\033[0m";
if ($exit == "oui" || $exit == "o" || $exit == "yes" || $exit == "y") {
    goto choix;
} elseif ($exit == "non" || $exit == "n" || $exit == "no") {
    goto exercice2_4;
} else {
    echo "\033[31mErreur.\033[0m\n";
    goto reboot2_4;
}
//

/* Début du code de l'exercice 3 (divisible) */
exercice3:

echo "\n\033[34mVeuillez choisir un nombre.\033[0m\n\n";

echo "\033[32m";

$nombre_utilisateur = intval(readline("Nombre : "));

echo "\033[0m";

if (($nombre_utilisateur % 3) == 0) {
    echo "\nVotre nombre \033[32m" . $nombre_utilisateur . "\033[0m est divisible par 3.\n";
} else {
    echo "\n\033[31mVotre nombre " . $nombre_utilisateur . " n'est pas divisible par 3 !\033[0m\n";
}
/* Fin du code de l'exercice 3 (divisible) */

/* ou is_float($nombre_utilisateur) */

reboot3:
// Redémarrage ou non de l'exercice 3 (divisible)
echo "\n\033[34mSouhaitez-vous quitter l'exercice 3 (divisible) ?\033[0m (\033[32moui\033[0m / \033[32mnon\033[0m)\n\n";
echo "\033[32m";
$exit = readline("\nRéponse : ");
echo "\033[0m";
if ($exit == "oui" || $exit == "o" || $exit == "yes" || $exit == "y") {
    goto choix;
} elseif ($exit == "non" || $exit == "n" || $exit == "no") {
    goto exercice3;
} else {
    echo "\033[31mErreur.\033[0m\n";
    goto reboot3;
}
//

/* Début du code de l'exercice 4 (nombre premier) */
exercice4:

echo "\n\033[34mVeuillez choisir un nombre.\033[0m\n";

echo "\033[32m";

$nombre_utilisateur = intval(readline("Nombre : "));

echo "\033[0m";

$divisible = 0;

for ($i = 2; $i < $nombre_utilisateur; $i++) {
    //test du quotient de la division
    if ($nombre_utilisateur % $i == 0) {
        // vérifications
        //echo "Nombre : " . $nombre_utilisateur . " - i : " . $i . " - Nombre/i : " . ($nombre_utilisateur / $i) . " - Quotient : " . ($nombre_utilisateur % $i) . "\n";
        $divisible++;
    }
}

if ($divisible > 1) {
    echo "Votre nombre \033[32m" . $nombre_utilisateur . "\033[0m n'est pas un nombre premier !\n";
} else {
    echo "Votre nombre \033[32m" . $nombre_utilisateur . "\033[0m est un nombre premier !\n";
}

/* Fin du code de l'exercice 4 (nombre premier) */

reboot4:
// Redémarrage ou non de l'exercice 4 (nombre premier)
echo "\n\033[34mSouhaitez-vous quitter l'exercice 4 (nombre premier) ?\033[0m (\033[32moui\033[0m / \033[32mnon\033[0m)\n\n";
echo "\033[32m";
$exit = readline("\nRéponse : ");
echo "\033[0m";
if ($exit == "oui" || $exit == "o" || $exit == "yes" || $exit == "y") {
    goto choix;
} elseif ($exit == "non" || $exit == "n" || $exit == "no") {
    goto exercice4;
} else {
    echo "\033[31mErreur.\033[0m\n";
    goto reboot4;
}
//

/* Début du code de l'exercice 5.1 (table de multiplications 1) */
exercice5_1:

echo "\nVoici la table de multiplications de 1 et de 6.\n\n";

$nombre1 = 1;
$nombre2 = 6;

echo "Table de multiplications de 1 :\n";

for ($compteur = 1; $compteur <= 10; $compteur++) {
    echo $compteur . " x " . $nombre1 . " = " . ($compteur*$nombre1) . "\n";
}

echo "\nTable de multiplications de 6 :\n";

for ($compteur = 1; $compteur <= 10; $compteur++) {
    echo $compteur . " x " . $nombre2 . " = " . ($compteur*$nombre2) . "\n";
}

/* Fin du code de l'exercice 5.1 (table de multiplications 1) */

reboot5_1:
// Redémarrage ou non de l'exercice 5.1 (table de multiplications 1)
echo "\n\033[34mSouhaitez-vous quitter l'exercice 5.1 (table de multiplications 1) ?\033[0m (\033[32moui\033[0m / \033[32mnon\033[0m)\n\n";
echo "\033[32m";
$exit = readline("\nRéponse : ");
echo "\033[0m";
if ($exit == "oui" || $exit == "o" || $exit == "yes" || $exit == "y") {
    goto choix;
} elseif ($exit == "non" || $exit == "n" || $exit == "no") {
    goto exercice5_1;
} else {
    echo "\033[31mErreur.\033[0m\n";
    goto reboot5_1;
}
//

/* Début du code de l'exercice 5.2 (table de multiplications 2) */
exercice5_2:

echo "\n\033[34mVeuillez choisir un nombre.\033[0m\n";

echo "\033[32m";

$nombre_utilisateur = intval(readline("Nombre : "));

echo "\033[0m";

echo "Table de multiplications de " . $nombre_utilisateur . " :\n";

for ($compteur = 1; $compteur <= 10; $compteur++) {
    echo $compteur . " x " . $nombre_utilisateur . " = " . ($compteur*$nombre_utilisateur) . "\n";
}

/* Fin du code de l'exercice 5.2 (table de multiplications 2) */

reboot5_2:
// Redémarrage ou non de l'exercice 5.2 (table de multiplications 2)
echo "\n\033[34mSouhaitez-vous quitter l'exercice 5.2 (table de multiplications 2) ?\033[0m (\033[32moui\033[0m / \033[32mnon\033[0m)\n\n";
echo "\033[32m";
$exit = readline("\nRéponse : ");
echo "\033[0m";
if ($exit == "oui" || $exit == "o" || $exit == "yes" || $exit == "y") {
    goto choix;
} elseif ($exit == "non" || $exit == "n" || $exit == "no") {
    goto exercice5_2;
} else {
    echo "\033[31mErreur.\033[0m\n";
    goto reboot5_2;
}
//

/* Début du code de l'exercice 5.3 (table de multiplications 3) */
exercice5_3:

echo "\nVoici les tables de multiplications de 1 à 10.\n";



for ($nombre = 1; $nombre <= 10; $nombre++) {
    echo "Table de multiplications de " . $nombre . " :\n";

    for ($compteur = 1; $compteur <= 10; $compteur++) {
        echo $compteur . " x " . $nombre . " = " . ($compteur*$nombre) . "\n";
    }

}

/* Fin du code de l'exercice 5.3 (table de multiplications 3) */

reboot5_3:
// Redémarrage ou non de l'exercice 5.3 (table de multiplications 3)
echo "\n\033[34mSouhaitez-vous quitter l'exercice 5.3 (table de multiplications 3) ?\033[0m (\033[32moui\033[0m / \033[32mnon\033[0m)\n\n";
echo "\033[32m";
$exit = readline("\nRéponse : ");
echo "\033[0m";
if ($exit == "oui" || $exit == "o" || $exit == "yes" || $exit == "y") {
    goto choix;
} elseif ($exit == "non" || $exit == "n" || $exit == "no") {
    goto exercice5_3;
} else {
    echo "\033[31mErreur.\033[0m\n";
    goto reboot5_3;
}
//

/* Début du code de l'exercice 6 (#) */
exercice6:

for ($i = 0; $i <= 50; $i++) {
    $diese = "#";
    echo str_repeat($diese, $i) . "\n";
}
/* Fin du code de l'exercice 6 (#) */

reboot6:
// Redémarrage ou non de l'exercice 6 (#)
echo "\n\033[34mSouhaitez-vous quitter l'exercice 8 (#) ?\033[0m (\033[32moui\033[0m / \033[32mnon\033[0m)\n\n";
echo "\033[32m";
$exit = readline("\nRéponse : ");
echo "\033[0m";
if ($exit == "oui" || $exit == "o" || $exit == "yes" || $exit == "y") {
    goto choix;
} elseif ($exit == "non" || $exit == "n" || $exit == "no") {
    goto exercice6;
} else {
    echo "\033[31mErreur.\033[0m\n";
    goto reboot6;
}
//

/* Début du code de l'exercice 7 (suite de Fibonacci) */
exercice7:

echo "\nVoici les nombres de la suite de Fibonacci inférieur à 5000 :\n";

$nombre_utilisateur = 5000;
$nombre1 = 0;
$nombre2 = 1;
$nombre3 = 0;

// merci à David pour l'ordre des calculs
while ($nombre3 < $nombre_utilisateur) {
    echo $nombre3 . "\n";
    $nombre3 = $nombre1 + $nombre2;
    $nombre2 = $nombre1;
    $nombre1 = $nombre3;
}
echo "\n";

// $a+=1; ==> $a = $a+1;
/* Fin du code de l'exercice 7 (suite de Fibonacci) */

reboot7:
// Redémarrage ou non de l'exercice 7 (suite de Fibonacci)
echo "\n\033[34mSouhaitez-vous quitter l'exercice 7 (suite de Fibonacci) ?\033[0m (\033[32moui\033[0m / \033[32mnon\033[0m)\n\n";
echo "\033[32m";
$exit = readline("\nRéponse : ");
echo "\033[0m";
if ($exit == "oui" || $exit == "o" || $exit == "yes" || $exit == "y") {
    goto choix;
} elseif ($exit == "non" || $exit == "n" || $exit == "no") {
    goto exercice7;
} else {
    echo "\033[31mErreur.\033[0m\n";
    goto reboot7;
}
//

/* Début du code de l'exercice 8 (20 premiers nombre premiers) */
exercice8:

echo "\nVoici la liste des 20 premiers nombres premiers :\n";
$nombre_a_chercher = 11;
$limite = 20;
$divisible = 0;

// count = 20;

for ($i = 2; $i < $nombre_a_chercher; $i++) {
    //test du quotient de la division
    if ($nombre_a_chercher % $i == 0) {
        echo $nombre_a_chercher . "\n";
        $divisible++;
    }
}

if ($divisible > 1) {
    echo "Le nombre " . $nombre_a_chercher . "n'est pas un nombre premier !";
} else {
    echo $nombre_a_chercher . "\n";
}

/* Fin du code de l'exercice 8 (20 premiers nombre premiers) */

reboot8:
// Redémarrage ou non de l'exercice 8 (20 premiers nombre premiers)
echo "\n\033[34mSouhaitez-vous quitter l'exercice 8 (20 premiers nombre premiers) ?\033[0m (\033[32moui\033[0m / \033[32mnon\033[0m)\n\n";
echo "\033[32m";
$exit = readline("\nRéponse : ");
echo "\033[0m";
if ($exit == "oui" || $exit == "o" || $exit == "yes" || $exit == "y") {
    goto choix;
} elseif ($exit == "non" || $exit == "n" || $exit == "no") {
    goto exercice8;
} else {
    echo "\033[31mErreur.\033[0m\n";
    goto reboot8;
}
//

// Script neutre
/* Début du code de l'exercice XXX */
//exerciceXXX:

//echo "\nXXX\n";

/* Fin du code de l'exercice XXX */

//rebootXXX:
// Redémarrage ou non de l'exercice XXX
//echo "\n\033[34mSouhaitez-vous quitter l'exercice XXX ?\033[0m (\033[32moui\033[0m / \033[32mnon\033[0m)\n\n";
//echo "\033[32m";
//$exit = readline("\nRéponse : ");
//echo "\033[0m";
//if ($exit == "oui" || $exit == "o" || $exit == "yes" || $exit == "y") {
//    goto choix;
//} elseif ($exit == "non" || $exit == "n" || $exit == "no") {
//    goto exerciceXXX;
//} else {
//    echo "\033[31mErreur.\033[0m\n";
//    goto rebootXXX;
//}
//

?>
